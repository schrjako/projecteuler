utils = __import__('000_utils')
N = 1000000
P = utils.Prime(N + 1, True)
M = 0
ans = 1
for i in range(2, N + 1):
    phi = i / P.phi(i)
    if phi > M:
        M = phi
        ans = i
print(ans)
