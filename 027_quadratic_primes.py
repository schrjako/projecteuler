utils = __import__("000_utils")

p = utils.Prime(1000000).get_bool_arr()

maxn = 0
for a in range(-999, 1000):
    for b in range(-1000, 1001):
        n = 0
        while p[n**2 + a*n + b]:
            n += 1
        if n > maxn:
            maxn = n
            ans = a * b
print(ans)
