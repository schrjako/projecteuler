grid = [[0 for _ in range(21)] for _ in range(21)]

grid[0][0] = 1

for i in range(21):
    for j in range(21):
        if i > 0:
            grid[i][j] += grid[i-1][j]
        if j > 0:
            grid[i][j] += grid[i][j-1]

print(grid[20][20])
