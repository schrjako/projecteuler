triangle = []

with open('067.in', 'r') as f:
    for line in f:
        triangle.append(line.replace('  ', ' ').strip().split(' '))

for i in range(len(triangle)):
    for j in range(len(triangle[i])):
        triangle[i][j] = int(triangle[i][j])
        if i > 0:
            if j == 0:
                triangle[i][j] += triangle[i - 1][j]
            elif j == i:
                triangle[i][j] += triangle[i - 1][j - 1]
            else:
                triangle[i][j] += max(triangle[i - 1][j - 1],
                                      triangle[i - 1][j])

ans = 0
for i in range(len(triangle[len(triangle) - 1])):
    ans = max(ans, triangle[len(triangle) - 1][i])
print(ans)
