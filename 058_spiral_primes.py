utils = __import__("000_utils")
N = 1000000
P = utils.Prime(N)
primes = P.get_primes_arr()
i = 1
off = 2
cnt = 0
num = 1
while 10 * cnt >= num or i == 1:
    num += 4
    for _ in range(4):
        i += off
        if P.is_prime(i):
            cnt += 1
    off += 2
print(off - 1)
