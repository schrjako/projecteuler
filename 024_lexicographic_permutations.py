from math import factorial

n = 1000000

digits = [str(i) for i in range(10)]
ans = ''

for j in range(10):
    i = 0
    while n > (i + 1) * factorial(9-j):
        i += 1
    n -= i * factorial(9-j)
    ans += digits[i]
    digits.remove(digits[i])

ans += "".join(digits)
print(ans)
