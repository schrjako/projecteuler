from itertools import product
N = 50
coords = [[i, j] for i, j in product(range(N + 1), range(N + 1))]


def check(P, Q):
    if P == Q or P == [0, 0] or Q == [0, 0]:
        return
    arr = [P, Q, [0, 0]]
    for i in range(3):
        j = (i + 1) % 3
        k = (i + 2) % 3
        if arr[i][0] == arr[j][0] and arr[j][1] == arr[k][1] or \
           arr[i][1] == arr[j][1] and arr[j][0] == arr[k][0]:
            return True
    for i in range(3):
        j = (i + 1) % 3
        k = (i + 2) % 3
        if arr[i][0] != arr[j][0] and arr[i][1] != arr[j][1] \
            and arr[j][0] != arr[k][0] and arr[j][1] != arr[k][1] \
            and ((arr[i][1] - arr[j][1]) * (arr[j][1] - arr[k][1]) ==
                 (arr[j][0] - arr[k][0]) * (arr[i][0] - arr[j][0]) * -1):
            return True
    return False


cnt = 0
for P, Q in product(coords, coords):
    if check(P, Q):
        cnt += 1
print(cnt // 2)
