from fractions import Fraction
N = 100


def get_e(n):
    if n == 1:
        ret = 2 + Fraction(1, get_e(n+1))
    elif n == N:
        ret = Fraction(1, 1 if n % 3 != 0 else 2 * n // 3)
    else:
        ret = (1 if n % 3 != 0 else 2 * n // 3) + Fraction(1, get_e(n+1))
    return ret


print(sum([int(i) for i in str(get_e(1).numerator)]))
