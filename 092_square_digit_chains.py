import sys
import resource
sys.setrecursionlimit(10000000)
resource.setrlimit(resource.RLIMIT_STACK, (2**29, -1))
utils = __import__('000_utils')
v = [-1] * 10000000
v[1] = 0
v[89] = 1


def dfs(num):
    if v[num] == -1:
        v[num] = dfs(utils.func_digit_sum(num, lambda n: n ** 2))
    return v[num]


print(sum(map(dfs, range(1, 10000000))))
