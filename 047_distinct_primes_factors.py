utils = __import__("000_utils")
N = 200000
CON = 4
P = utils.Prime(N, True)
arr = [P.get_prime_factors(i) for i in range(N)]


def is_ok(num):
    for k in range(CON):
        if len(arr[num + k]) != CON:
            return False
    return True


for i in range(2, N):
    if is_ok(i):
        print(i)
        break
