from queue import PriorityQueue as PQ
with open('083.in') as f:
    m = [[*map(int, line.strip().split(','))] for line in f]
v = [[False] * len(m[0]) for _ in range(len(m))]
pq = PQ()
pq.put((0, [0, 0]))
b = [[0] * len(l) for l in m]
while pq.qsize() != 0:
    (d, pos) = pq.get()
    if pos[0] < 0 or pos[0] >= len(m) or pos[1] < 0 or pos[1] >= len(m[pos[0]]) or v[pos[0]][pos[1]]:
        continue
    d += m[pos[0]][pos[1]]
    b[pos[0]][pos[1]] = d
    v[pos[0]][pos[1]] = True
    for k in [[0, 1], [1, 0], [-1, 0], [0, -1]]:
        pq.put((d, [k[0] + pos[0], k[1] + pos[1]]))
print(b[-1][-1])
