utils = __import__("000_utils")
ans = 0
for i in range(1000000):
    if utils.is_palindrome(i) and utils.is_palindrome("{0:b}".format(i)):
        ans += i
print(ans)
