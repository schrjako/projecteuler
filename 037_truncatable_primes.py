utils = __import__("000_utils")
p = utils.Prime(1000000).get_bool_arr()
ans = 0
cnt = 0
i = 10


def truncatableL(num):
    while num > 9:
        num = int(str(num)[1:])
        if not p[num]:
            return False
    return True


def truncatableR(num):
    while num > 9:
        num = int(str(num)[:-1])
        if not p[num]:
            return False
    return True


while cnt < 11:
    if p[i] and truncatableL(i) and truncatableR(i):
        ans += i
        cnt += 1
    i += 1
print(ans)
