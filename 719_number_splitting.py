utils = __import__('000_utils')
ans = 0
N = 10 ** 6
for i in range(N + 1):
    if utils.s_number(i ** 2, i):
        ans += i ** 2
print(ans)
