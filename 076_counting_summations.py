N = 100
dp = [1] + [0] * N
for i in range(1, N + 1):
    for j in range(N + 1 - i):
        dp[j + i] += dp[j]
print(dp[N] - 1)
