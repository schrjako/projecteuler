ans = 0


def dfs(num):
    global ans
    if len(num) == 10:
        ans += int(num)
        return
    for digit in "0123456789":
        if digit in num:
            continue
        cnum = num + digit
        if len(cnum) > 3 and \
           int(cnum[-3:]) % [2, 3, 5, 7, 11, 13, 17][len(cnum) - 4] != 0:
            continue
        dfs(cnum)


dfs('')
print(ans)
