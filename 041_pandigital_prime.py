utils = __import__("000_utils")
ans = 0
N = 10000000
p = utils.Prime(N).get_bool_arr()
for i in range(N):
    if p[i] and utils.is_pandigital(i):
        ans = i
print(ans)
