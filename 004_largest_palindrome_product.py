ans = 0
for i in range(100, 1000):
    for j in range(i, 1000):
        num = i*j
        if str(num) == str(num)[::-1]:
            ans = max(ans, num)

print(ans)
