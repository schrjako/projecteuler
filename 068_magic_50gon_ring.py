N = 10
L = [16]


def perm(nums):
    if len(nums) == N:
        S = nums[0] + nums[1] + nums[N - 1]
        for i in range(1, N - 2, 2):
            if sum(nums[i:i+3]) != S:
                return -1
        m = 0
        for i in range(0, N - 1, 2):
            if nums[i] < nums[m]:
                m = i
        ans = ''
        for i in range(N // 2):
            ans += str(nums[(m + 2 * i) % N]) + \
                   str(nums[(m + 2 * i + N - 1) % N]) + \
                   str(nums[(m + 2 * i + 1) % N])
        if len(ans) in L:
            return int(ans)
        return -1
    ret = -1
    for i in range(1, N + 1):
        if i in nums:
            continue
        nums.append(i)
        ret = max(ret, perm(nums))
        nums.remove(i)
    return ret


print(perm([]))
