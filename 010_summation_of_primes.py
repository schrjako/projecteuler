N = 2000000
p = [1 for i in range(N)]

for i in range(2, N):
    if p[i] == 1:
        j = 2
        while j*i < N:
            p[i*j] = 0
            j += 1

ans = 0
for i in range(2, N):
    if p[i] == 1:
        ans += i

print(ans)
