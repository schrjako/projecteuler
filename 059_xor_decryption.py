utils = __import__('000_utils')


def decrypt(nums, key):
    message = ''
    for i in range(len(nums)):
        message += chr(int(nums[i]) ^ ord(key[i % 3]))
    return message


def contains(string, chars):
    for char in chars:
        if char in string:
            return True
    return False


with open('059.in') as f:
    nums = f.read().strip().split(',')

abc = 'abcdefghijklmnopqrstuvwxyz'
for key in [a + b + c for a in abc for b in abc for c in abc]:
    decrypted = decrypt(nums, key)
    if not contains(decrypted, "&#{}*`"):
        ans = utils.array_sum(map(ord, [char for char in decrypted]))
print(ans)
