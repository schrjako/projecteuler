from math import log

with open('099.in') as f:
    M = 0.0
    ans = 0
    i = 0
    for line in f:
        x, y = line.strip().split(',')
        i += 1
        val = int(y) * log(int(x))
        if val > M:
            M = val
            ans = i
print(ans)
