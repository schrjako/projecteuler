utils = __import__("000_utils")
i = 40756
while not utils.is_pentagonal(utils.triangle(i)) or \
      not utils.is_hexagonal(utils.triangle(i)):
    i += 1
print(utils.triangle(i))
