sq2 = 2 ** ( 1 / 2)
tsqt = 2 * sq2

n = 0
while True:
    x = int(round(abs(1 / 8 * ( (2 + sq2) * ( ( 3 - tsqt) ** n) + ( 2 - sq2 ) * ( ( 3 + tsqt) ** n) + 4)), 0))
    y = int(round(abs(1 / 4 * ( (-1 - sq2) * ( ( 3 - tsqt) ** n) + ( sq2 - 1 ) * ( ( 3 + tsqt) ** n) + 2)), 0))
    if y > 1_000_000_000_000:
        print(x)
        exit()
    n += 1
