from fractions import Fraction

ans = Fraction(0)
L = Fraction(0)
R = Fraction(1)
while L.denominator <= 1000000:
    ans = max(ans, L)
    M = Fraction(L.numerator + R.numerator, L.denominator + R.denominator)
    if M < Fraction(3, 7):
        L = M
    else:
        R = M

print(ans.numerator)
