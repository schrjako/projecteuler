from numpy import gcd

denom = 1
numer = 1

for i in range(10, 100):
    for j in range(i + 1, 100):
        for k in range(2):
            for L in range(2):
                if str(i)[k] == str(j)[L] and \
                   i*int(str(j)[1-L]) == int(str(i)[1-k])*j and \
                   not (i % 10 == 0 and j % 10 == 0):
                    numer *= i
                    denom *= j

print(denom//gcd(denom, numer))
