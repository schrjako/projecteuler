from fractions import Fraction

ans = 0
f = Fraction(1)
for i in range(1000):
    f = Fraction(1, 1 + f) + 1
    if len(str(f.numerator)) > len(str(f.denominator)):
        ans += 1
print(ans)
