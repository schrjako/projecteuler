from fractions import Fraction
import sys
import resource
utils = __import__('000_utils')
resource.setrlimit(resource.RLIMIT_STACK, (2**29, -1))
N = 12000
sys.setrecursionlimit(1000000)
memo = {}


def dfs(L, R):
    if L.denominator > N or R.denominator > N:
        return 0
    s = "{}:{}".format(min(L.denominator, R.denominator),
                       max(L.denominator, R.denominator))
    if s in memo:
        return memo[s]
    M = Fraction(L.numerator + R.numerator, L.denominator + R.denominator)
    memo[s] = 1 if M.denominator <= N else 0
    memo[s] += dfs(L, M)
    memo[s] += dfs(M, R)
    return memo[s]


print(dfs(Fraction(1, 3), Fraction(1, 2)))
