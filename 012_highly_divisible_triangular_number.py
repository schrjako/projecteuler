def numdivisors(num):
    ret = 1
    i = 2
    while i**2 < num:
        curr = 1
        while num % i == 0:
            curr += 1
            num //= i
        ret *= curr
        i += 1
    if num != 1:
        ret *= 2
    return ret


for i in range(100000000):
    if numdivisors(i*(i+1)//2) > 500:
        print(i*(i+1)//2)
        break
