fib = [0, 1, 1]
ans = 0

while fib[2] <= 4000000:
    if fib[2] % 2 == 0:
        ans += fib[2]
    fib = fib[1:]
    fib.append(sum(fib))

print(ans)
