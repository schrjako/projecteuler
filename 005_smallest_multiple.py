from numpy import lcm

ans = 1

for i in range(1, 21):
    ans = lcm(ans, i)

print(ans)
