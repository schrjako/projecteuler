from math import factorial
ans = 0
for i in range(10, 10000000):
    curr = 0
    for digit in str(i):
        curr += factorial(int(digit))
    if curr == i:
        ans += i
print(ans)
