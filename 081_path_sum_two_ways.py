with open('081.in') as f:
    m = [line.strip().split(',') for line in f]
b = []
for i in range(len(m)):
    b.append([])
    for j in range(len(m[i])):
        b[-1].append(int(m[i][j]))
        if i != 0 and j != 0:
            b[i][j] += min(b[i-1][j], b[i][j-1])
        elif i != 0:
            b[i][j] += b[i-1][j]
        elif j != 0:
            b[i][j] += b[i][j-1]
print(b[-1][-1])
