from math import factorial

ans = 0
for digit in str(factorial(100)):
    ans += int(digit)
print(ans)
