utils = __import__("000_utils")

ans = 0
for i in range(1, 100000):
    curr = str(i)
    f = 2
    while len(curr) < 9:
        curr += str(i * f)
        f += 1
    if len(curr) == 9 and utils.is_n_pandigital(curr, 9):
        ans = max(ans, int(curr))
print(ans)
