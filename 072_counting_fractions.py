utils = __import__('000_utils')
N = 1000000
P = utils.Prime(N+1, True)
ans = 0
for i in range(2, N+1):
    ans += P.phi(i)
print(ans)
