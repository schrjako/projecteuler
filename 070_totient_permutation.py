utils = __import__('000_utils')
N = 10000000
P = utils.Prime(N, True)
M = 10000000
ans = 1
DIGITS = '1234567890'
for i in range(2, N):
    phi = P.phi(i)
    if utils.countchars(i, DIGITS) == utils.countchars(phi, DIGITS) and i / phi < M:
        M = i / phi
        ans = i
print(ans)
