pan_product = set()


def count(num):
    for i in range(1, 7):
        for j in range(i+1, 8):
            if int(num[:i]) * int(num[i:j]) == int(num[j:]):
                pan_product.add(int(num[j:]))


def dfs(num):
    if len(num) == 9:
        count(num)
        return
    for digit in '123456789':
        if digit not in num:
            dfs(num + digit)


dfs('')
ans = 0
for num in pan_product:
    ans += num
print(ans)
