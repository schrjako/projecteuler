utils = __import__('000_utils')
N = 50000000
P = utils.Prime(round(N ** (1 / 2)))
primes = P.get_primes_arr()
p = [2, 2, 2]
v = set()
for p[0] in primes:
    sq = p[0] ** 2
    if sq >= N:
        break
    for p[1] in primes:
        cub = p[1] ** 3
        if sq + cub >= N:
            break
        for p[2] in primes:
            quad = p[2] ** 4
            if sq + cub + quad >= N:
                break
            v.add(sq + cub + quad)
print(len(v))
