utils = __import__("000_utils")


def changes(a, b):
    return str(sorted([([i for i in range(len(a)) if a[i] == c], [i for i in range(len(b)) if b[i] == c]) for c in set(a)]))


ans = 0
char_nums = {}
nums = []
with open("098.in") as f:
    for word in map(lambda l: l.strip(), f.readlines()):
        char_num = str([(chr(i), word.count(chr(i))) for i in range(ord('A'), ord('Z')+1) if word.count(chr(i)) > 0])
        if char_num in char_nums:
            for other in char_nums[char_num]:
                nums.append(changes(other, word) + str(sorted([word.count(chr(i)) for i in range(ord('A'), ord('Z')+1) if word.count(chr(i)) > 0])))
                nums.append(changes(word, other) + str(sorted([word.count(chr(i)) for i in range(ord('A'), ord('Z')+1) if word.count(chr(i)) > 0])))
            char_nums[char_num].append(word)
        else:
            char_nums[char_num] = [word]

sq_nums = {}
for i in range(2, 10000):
    num = str(i ** 2)
    dig_num = str([(i, num.count(str(i))) for i in range(10) if num.count(str(i)) > 0])
    if dig_num in sq_nums:
        for other in sq_nums[dig_num]:
            if changes(other, num) + str(sorted([num.count(str(i)) for i in range(10) if num.count(str(i)) > 0])) in nums:
                ans = i ** 2
        sq_nums[dig_num].append(num)
    else:
        sq_nums[dig_num] = [num]

print(ans)
