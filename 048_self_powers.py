utils = __import__("000_utils")
ans = 0
mod = 10000000000
for i in range(1, 1001):
    ans += utils.fast_mod_pow(i, i, mod)
print(ans % mod)
