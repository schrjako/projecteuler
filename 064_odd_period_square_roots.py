utils = __import__("000_utils")

ans = 0
for i in range(2, 10001):
    if len(utils.get_sqrt_approximations(i)[2]) % 2 == 1:
        ans += 1
print(ans)
