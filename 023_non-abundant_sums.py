utils = __import__("000_utils")

abundant_nums = []
for i in range(1, 28124):
    if utils.sum_of_proper_divisors(i) > i:
        abundant_nums.append(i)
expressed = [False for _ in range(28124)]

for i in abundant_nums:
    for j in abundant_nums:
        if i + j <= 28123:
            expressed[i + j] = True

ans = 0
for i in range(28124):
    if not expressed[i]:
        ans += i
print(ans)
