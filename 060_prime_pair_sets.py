utils = __import__("000_utils")
N = 8400
P = utils.Prime(N)
primes = P.get_primes_arr()
neighbours = []
ans = N ** 2


def are_neighbours(p1, p2):
    return P.is_prime(int(str(p2) + str(p1))) and \
           P.is_prime(int(str(p1) + str(p2)))


def dfs(num, p, start):
    if len(p) == 4:
        global ans
        ans = min(ans, utils.array_sum(p) + primes[num])
        return
    if utils.array_sum(p) + primes[num] >= ans:
        return ans
    for i in range(start, len(neighbours[num])):
        prime = neighbours[num][i]
        if prime + utils.array_sum(p) + primes[num] >= ans:
            break
        ok = True
        for p2 in p:
            if not are_neighbours(p2, prime):
                ok = False
        if not ok:
            continue
        dfs(num, p.copy() + [prime], i + 1)


for i in range(len(primes)):
    p1 = primes[i]
    neighbours.append([p1])
    for j in range(i+1, len(primes)):
        p2 = primes[j]
        if are_neighbours(p1, p2):
            neighbours[-1].append(p2)

for i in range(len(primes)):
    if len(neighbours[i]) >= 4:
        dfs(i, [], 1)
print(ans)
