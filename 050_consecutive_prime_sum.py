utils = __import__("000_utils")
N = 1000000
P = utils.Prime(N)
bp = P.get_bool_arr()
primes = P.get_primes_array()
maxlen = 0
ans = 0
while len(primes):
    s = 0
    i = 0
    for prime in primes:
        s += prime
        i += 1
        if s >= N:
            break
        if bp[s] and i > maxlen:
            maxlen = i
            ans = s
    primes = primes[1:]
print(ans)
