with open('082.in') as f:
    m = [[*map(int, line.strip().split(','))] for line in f]
b = [l[:1] + [1000000000] * (len(l) - 1) for l in m]
for j in range(1, len(m)):
    b[0][j] = b[0][j-1] + m[0][j]
    for i in range(1, len(m[j])):
        b[i][j] = m[i][j] + min(b[i-1][j], b[i][j-1])
    for i in range(len(m[j]) - 2, -1, -1):
        b[i][j] = min(b[i][j], b[i+1][j] + m[i][j])
print(min(map(lambda r: r[-1], b)))
