from itertools import product as pr, permutations as perm, combinations as comb
M = 0
ans = ''


def ceval(digits, order, oper):
    for i in range(3):
        j = order.index(i)
        if oper[j] == '/' and digits[j + 1] == 0:
            return -1
        c = eval(str(digits[j]) + oper[j] + str(digits[j + 1]))
        digits = digits[:j] + [c] + digits[j + 2:]
        order.remove(i)
        oper = oper[:j] + oper[j + 1:]
    if abs(round(digits[0]) - digits[0]) < 1e-9:
        return round(digits[0])
    return -1


def cnt(s):
    s.sort()
    for i in range(len(s)):
        if s[i] != i:
            return i - 1
    return len(s) - 1


for digits in comb(range(1, 10), r=4):
    s = set([0])
    for cdigits, order, oper in pr(perm(digits), perm(range(3)), pr('+-*/', repeat=3)):
        c = ceval(list(cdigits), list(order), list(oper))
        if c >= 0:
            s.add(c)
    m = cnt(list(s))
    if m > M:
        M = m
        ans = ''.join(map(str, digits))

print(ans)
