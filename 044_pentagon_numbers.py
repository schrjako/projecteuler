utils = __import__("000_utils")

d = 1000000000
for j in range(2, 10000):
    pj = utils.pentagonal(j)
    for k in range(1, j):
        pk = utils.pentagonal(k)
        if utils.is_pentagonal(pj + pk) and utils.is_pentagonal(pj - pk):
            d = min(d, pj - pk)
print(d)
