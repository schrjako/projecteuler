with open('079.in') as f:
    nums = set(f.read().strip().split('\n'))
    chars = ''.join(set(c for c in ''.join(nums)))
    pov = [[False for _ in range(10)] for _ in range(10)]
    for num in nums:
        pov[int(num[0])][int(num[1])] = True
        pov[int(num[1])][int(num[2])] = True


s = ''
for _ in range(len(chars)):
    for c in chars:
        if c in s:
            continue
        if sum(pov[int(c)]) == 0:
            s = c + s
            for i in range(10):
                pov[i][int(c)] = False
            break
print(s)
