VALUES = "23456789TJQKA"
SUITS = "HCSD"


def get_hand(hand):
    return [[hand[i][j] for i in range(5)] for j in range(2)]


def get_colors(hand):
    ret = {i: [] for i in range(6)}
    for char in SUITS:
        ret[hand[1].count(char)].append(char)
    return ret


def get_values(hand):
    ret = {i: [] for i in range(6)}
    for char in VALUES:
        ret[hand[0].count(char)].append(char)
    return ret


def is_pair(hand):
    values = get_values(hand)
    return len(values[2]) != 0, \
        -1 if len(values[2]) == 0 else VALUES.index(values[2][0])


def is_two_pairs(hand):
    values = get_values(hand)
    return len(values[2]) == 2, \
        -1 if len(values[2]) != 2 else VALUES.index(values[2][0]), \
        -1 if len(values[2]) != 2 else VALUES.index(values[2][1])


def is_three_of_a_kind(hand):
    values = get_values(hand)
    return len(values[3]) == 1, \
        -1 if len(values[3]) == 0 else values[3][0]


def is_straight(hand):
    i = 0
    while VALUES[i] not in hand[0]:
        i += 1
    for j in range(5):
        if i + j == len(VALUES) or VALUES[i + j] not in hand[0]:
            return False, -1
    return True, i


def is_flush(hand):
    colors = get_colors(hand)
    return len(colors[5]) == 1


def is_full_house(hand):
    is_p, pair = is_pair(hand)
    is_t, three = is_three_of_a_kind(hand)
    return is_p and is_t, three, pair


def is_four_of_a_kind(hand):
    values = get_values(hand)
    return len(values[4]) == 1, \
        -1 if len(values[4]) == 0 else values[4][0]


def is_straight_flush(hand):
    is_s, start = is_straight(hand)
    return is_s and is_flush(hand), start


def is_royal_flush(hand):
    is_sf, start = is_straight_flush(hand)
    return is_sf and 'A' in hand[0]


def cmp_values(hands):
    for value in VALUES[::-1]:
        if hands[0][0].count(value) > hands[1][0].count(value):
            return 1
        if hands[0][0].count(value) < hands[1][0].count(value):
            return -1


def gib_odgovor1(hands, func):
    b1 = func(hands[0])
    b2 = func(hands[1])
    if not b1 and not b2:
        return 0
    if b1 and not b2:
        return 1
    if not b1 and b2:
        return -1
    return cmp_values(hands)


def gib_odgovor2(hands, func):
    b1, s1 = func(hands[0])
    b2, s2 = func(hands[1])
    if not b1 and not b2:
        return 0
    if b1 and not b2:
        return 1
    if not b1 and b2:
        return -1
    if s1 != s2:
        return 1 if s1 > s2 else -1
    return cmp_values(hands)


def gib_odgovor3(hands, func):
    b1, s1, s2 = func(hands[0])
    b2, s3, s4 = func(hands[1])
    if not b1 and not b2:
        return 0
    if b1 and not b2:
        return 1
    if not b1 and b2:
        return -1
    if s1 != s3:
        return 1 if s1 > s3 else -1
    if s2 != s4:
        return 1 if s2 > s4 else -1
    return cmp_values(hands)


def wins(hands):
    rf = gib_odgovor1(hands, is_royal_flush)
    if rf != 0:
        return rf == 1

    sf = gib_odgovor2(hands, is_straight_flush)
    if sf != 0:
        return sf == 1

    foak = gib_odgovor2(hands, is_four_of_a_kind)
    if foak != 0:
        return foak == 1

    fh = gib_odgovor3(hands, is_full_house)
    if fh != 0:
        return fh == 1

    fl = gib_odgovor1(hands, is_flush)
    if fl != 0:
        return fl == 1

    st = gib_odgovor2(hands, is_straight)
    if st != 0:
        return st == 1

    toak = gib_odgovor2(hands, is_three_of_a_kind)
    if toak != 0:
        return toak == 1

    tp = gib_odgovor3(hands, is_two_pairs)
    if tp != 0:
        return tp == 1

    op = gib_odgovor2(hands, is_pair)
    if op != 0:
        return op == 1

    return cmp_values(hands) == 1


ans = 0
with open('054.in') as f:
    for line in f:
        hands = line.strip().split()
        curr = wins([get_hand(hands[:5]), get_hand(hands[5:])])
        if curr:
            ans += 1
print(ans)
