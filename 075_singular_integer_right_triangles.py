from math import gcd
utils = __import__('000_utils')
N = 1500000

opt = {}
v = {}


def check(a, b, c):
    arr = [a, b, c]
    arr.sort()
    [a, b, c] = arr
    if gcd(a, b, c) != 1:
        g = gcd(a, b, c)
        a //= g
        b //= g
        c //= g
    s = '{}:{}:{}'.format(a, b, c)
    if s in v:
        return
    v[s] = True
    k = 1
    while k * (a + b + c) <= N:
        o = k * (a + b + c)
        if o in opt:
            opt[o] += 1
        else:
            opt[o] = 1
        k += 1


m = 2
while 2 * m ** 2 <= N:
    for n in range(1, m):
        if gcd(n, m) != 1:
            continue
        check(m ** 2 - n ** 2, 2 * m * n, m ** 2 + n ** 2)
    m += 1
print(sum(filter(lambda num: num == 1, opt.values())))
