utils = __import__('000_utils')
U = 2000000
A = 0
D = 2000000
i = 1
ti = utils.triangle(i)
while ti <= U:
    j = 1
    tj = utils.triangle(j)
    while ti * tj <= U:
        if U - ti * tj < D:
            D = U - ti * tj
            A = i * j
        j += 1
        tj = utils.triangle(j)
    if ti * tj - U < D:
        D = ti * tj - U
        A = i * j
    i += 1
    ti = utils.triangle(i)
print(A)
