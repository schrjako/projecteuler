single = ['', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight',
          'nine', 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen',
          'sixteen', 'seventeen', 'eighteen', 'nineteen', 'twenty']
double = ['', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy',
          'eighty', 'ninety']

for i in range(len(single)):
    single[i] = len(single[i])
for i in range(len(double)):
    double[i] = len(double[i])


def num_letters(num):
    ret = 0

    if num % 100 <= 20:
        ret += single[num % 100]
        num //= 100
    else:
        ret += single[num % 10]
        num //= 10

        ret += double[num % 10]
        num //= 10

    if ret != 0 and num != 0:
        ret += len('and')
    if num % 10 != 0:
        ret += len('hundred') + single[num % 10]
    num //= 10

    if num != 0:
        ret += len('thousand') + single[num % 10]

    return ret


ans = 0
for i in range(1, 1001):
    ans += num_letters(i)
print(ans)
