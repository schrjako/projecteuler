from itertools import combinations
squares = ['{:02}'.format(i ** 2) for i in range(1, 10)]
for i in range(len(squares)):
    squares[i] = squares[i].replace('9', '6')
s = set()


def check(p):
    for sq in squares:
        if not ((int(sq[0]) in p[0] and int(sq[1]) in p[1]) or
                (int(sq[0]) in p[1] and int(sq[1]) in p[0])):
            sq2 = sq.replace('6', '9')
            if not ((int(sq2[0]) in p[0] and int(sq2[1]) in p[1]) or
                    (int(sq2[0]) in p[1] and int(sq2[1]) in p[0])):
                return
    cs = str(p)
    if cs not in s:
        s.add(str(p))


p = [1, 1]
for p[0] in combinations([i for i in range(10)], r=6):
    for p[1] in combinations([i for i in range(10)], r=6):
        check(p)
print(len(s) // 2)
