ans = -1
for j in range(1, 200000):
    s = set()
    for k in range(1, 7):
        s.add(str([digit * str(k * j).count(digit) for digit in '0123456789']))
    if len(s) == 1:
        print(j)
