utils = __import__("000_utils")
with open("042.in") as f:
    words = f.read().replace('"', '').strip().split(',')

triangle_numbers = [n*(n+1)//2 for n in range(1, 100)]

ans = 0
for word in words:
    if utils.word_value(word) in triangle_numbers:
        ans += 1
print(ans)
