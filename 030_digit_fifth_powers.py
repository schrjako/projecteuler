def get_pow(num):
    ret = 0
    for digit in str(num):
        ret += int(digit) ** 5
    return ret


ans = 0
for i in range(10, 1000000):
    if get_pow(i) == i:
        ans += i
print(ans)
