utils = __import__('000_utils')
num = {}
ans = 100000
for i in range(10000, 1, -1):
    counted = utils.countchars(i ** 3, '0123456789')
    if counted in num:
        num[counted].append(i)
        if len(num[counted]) == 5:
            ans = i
    else:
        num[counted] = [i]
print(ans ** 3)
