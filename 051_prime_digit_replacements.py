utils = __import__("000_utils")
N = 1000000
primes = utils.Prime(N).get_primes_arr()
primes.reverse()
num = {}
ans = 0
for prime in primes:
    for digit in '0123456789':
        if digit not in str(prime):
            continue
        keypart = ('L' + str(prime) + 'R').split(digit)
        for i in range(1, 2 ** (len(keypart) - 1)):
            k = i
            key = keypart[0][1:]
            for j in range(len(keypart) - 1):
                if k % 2 == 1:
                    key += '*'
                else:
                    key += digit
                key += keypart[j + 1]
                k //= 2
            key = key[:-1]
            if key not in num.keys():
                num[key] = 0
            num[key] += 1
            if num[key] >= 8:
                ans = prime
print(ans)
