utils = __import__("000_utils")

ans = 0
for a in range(1, 100):
    for b in range(1, 100):
        ans = max(ans, utils.digit_sum(a**b))
print(ans)
