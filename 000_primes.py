from math import lcm


class Prime:

    def __init__(self, N, needdivisors=False):
        self.N = N
        self.needdivisors = needdivisors
        if needdivisors:
            self.divisor = [0, 1] + [i + 2 for i in range(N-2)]
            for i in range(2, N):
                if self.divisor[i] == i:
                    j = 2
                    while j * i < N:
                        if self.divisor[i * j] == i * j:
                            self.divisor[i * j] = i
                        j += 1
        else:
            self.bool = [False, False] + [True] * (N - 2)
            i = 2
            while i ** 2 < N:
                if self.bool[i] == 1:
                    j = 2
                    while j*i < N:
                        self.bool[i*j] = False
                        j += 1
                i += 1
        self.primes = []
        for p in range(2, N):
            if self.needdivisors:
                if self.divisor[p] == p:
                    self.primes.append(p)
            else:
                if self.bool[p]:
                    self.primes.append(p)

    def get_bool_arr(self):
        return self.bool

    def get_divisors_arr(self):
        return self.divisor

    def get_primes_arr(self):
        return self.primes

    def get_prime_factors(self, num):
        s = set()
        while num > 1:
            s.add(self.divisor[num])
            num //= self.divisor[num]
        return s

    def __len__(self):
        return self.N

    def is_prime(self, p):
        if p < self.N:
            if self.needdivisors:
                return self.divisor[p] == p
            return self.bool[p]
        for prime in self.primes:
            if prime ** 2 > p:
                return True
            if p % prime == 0:
                return False
        i = self.primes[-1]
        while i ** 2 <= p:
            if p % i == 0:
                return False
            i += 1
        return True

    def get_primes_array(self, N=-1):
        if N == -1:
            return self.primes.copy()
        primes = []
        for i in range(N):
            if self.is_prime(i):
                primes.append(i)
        return primes

    def combinations(self, divisors, goal):
        ret = 0
        #   inclusion - exclusion principle
        for i in range(1, 2 ** len(divisors)):
            b = bin(i)[2:]
            b = '0' * (len(divisors) - len(b)) + b
            ret += ((-1) ** b.count('1')) * (goal // lcm(*(1 if b[j] == '0' else divisors[j] for j in range(len(divisors)))))
        return ret

    def phi(self, P):
        divisors = []
        p = P
        while p != 1:
            divisors.append(self.divisor[p])
            while divisors[-1] == self.divisor[p]:
                p //= divisors[-1]
        return P + self.combinations(divisors, P)
