utils = __import__("000_utils")

N = 1000000
p = utils.Prime(N).get_bool_arr()


def is_circular(num):
    if not p[num]:
        return False
    snum = str(num)
    for _ in range(len(str(num))-1):
        snum = str(snum)[1:] + str(snum)[0]
        if not p[int(snum)]:
            return False
    return True


ans = 0
for i in range(2, N):
    if is_circular(i):
        if '0' in str(i):
            print(i)
        ans += 1
print(ans)
