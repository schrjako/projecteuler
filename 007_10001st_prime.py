N = 1000000
p = [1 for i in range(N)]

for i in range(2, N):
    if p[i] == 1:
        j = 2
        while j*i < N:
            p[i*j] = 0
            j += 1

num = 0
for i in range(2, N):
    if p[i] == 1:
        num += 1
    if num == 10001:
        print(i)
        break
