from math import floor
utils = __import__('000_utils')


def get_digits(N):
    ans = floor(N ** (1 / 2))
    c = N - ans ** 2
    for _ in range(99):
        d = 0
        c *= 100
        while (2 * ans * 10 ** len(str(d)) + d) * d <= c:
            d += 1
        d -= 1
        c -= (2 * ans * 10 ** len(str(d)) + d) * d
        ans *= 10
        ans += d
    return ans


ans = 0
for i in range(1, 101):
    if not utils.is_square(i):
        ans += utils.digit_sum(get_digits(i))
print(ans)
