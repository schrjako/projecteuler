from itertools import product

fields = ['GO', 'A1', 'CC1', 'A2', 'T1', 'R1', 'B1', 'CH1', 'B2', 'B3', 'JAIL',
          'C1', 'U1', 'C2', 'C3', 'R2', 'D1', 'CC2', 'D2', 'D3', 'FP', 'E1',
          'CH2', 'E2', 'E3', 'R3', 'F1', 'F2', 'U2', 'F3', 'G2J', 'G1', 'G2',
          'CC3', 'G3', 'R4', 'CH3', 'H1', 'T2', 'H2']
CARDS = {
        'CC': ['GO', 'JAIL'],
        'CH': ['GO', 'JAIL', 'C1', 'E3', 'H2', 'R1', 'NR', 'NR', 'NU', '-3']
        }
dice_sides = 4
card_num = 16

prob = [[0 for _ in range(3)] for _ in range(len(fields))]
prob[0][0] = 1


def print_prob(pprob):
    print(' '.join(["{}: {:.4f}".format(fields[f], sum(pprob[f])) for f in range(len(fields))]))


for _ in range(100):
    n_prob = [[0 for d in range(3)] for f in range(len(fields))]
    for i, j in product(range(1, dice_sides + 1), repeat=2):
        is_double = i == j
        for f, d in product(range(40), range(3)):
            cprob = prob[f][d] / dice_sides ** 2
            if is_double and d == 2:
                n_prob[fields.index('JAIL')][0] += cprob
            else:
                cf = (f + i + j) % len(fields)
                cd = d + 1 if is_double else 0
                if fields[cf][:2] in ['CH', 'CC']:
                    ftype = fields[cf][:2]
                    n_prob[cf][cd] += cprob / card_num * (card_num - len(CARDS[ftype]))
                    for c in CARDS[ftype]:
                        if c in fields:
                            n_prob[fields.index(c)][cd] += cprob / card_num
                        elif c[0] == 'N':
                            ccf = (cf + list(map(lambda s: s[0], fields[cf:] + fields)).index(c[1])) % 40
                            n_prob[ccf][cd] += cprob / card_num
                        elif c == '-3':
                            n_prob[(cf - 3 + 40) % 40][cd] += cprob / card_num
                        else:
                            print("STH WENT WRONG, FOUND UNACCOUNTED CARD {}".format(c))
                elif fields[cf] == 'G2J':
                    n_prob[fields.index('JAIL')][cd] += cprob
                else:
                    n_prob[cf][cd] += cprob
    prob = n_prob


res = list(map(lambda t: '{:02d}'.format(t[1]), list(sorted([(sum(prob[f]), f, fields[f]) for f in range(len(fields))], reverse=True))[:3]))
print(''.join(res))
