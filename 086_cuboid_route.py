from math import gcd
utils = __import__('000_utils')
v = set()
N = 2000
cnt = [set() for i in range(N)]


def addopt(a, b, c):
    arr = [a, b, c]
    paths = [(arr[i] + arr[(i + 1) % 3]) ** 2 + arr[(i + 2) % 3] ** 2 for i in range(3)]
    if min(paths) != (a + b) ** 2 + c ** 2:
        return
    arr.sort()
    [a, b, c] = arr
    if c < N:
        cnt[c].add('{}:{}:{}'.format(a, b, c))


def add(a, b, c):
    arr = [a, b, c]
    arr.sort()
    [a, b, c] = arr
    if gcd(a, b, c) != 1:
        g = gcd(a, b, c)
        a //= g
        b //= g
        c //= g
    s = '{}:{}'.format(a, b)
    if s in v:
        return
    v.add(s)
    k = 1
    while k * min(a, b) < N:
        for i in range(1, k * a):
            addopt(i, k * a - i, k * b)
        for i in range(1, k * b):
            addopt(i, k * b - i, k * a)
        k += 1


m = 2
while m ** 2 <= N * 1.75:
    for n in range(1, m):
        if gcd(n, m) != 1:
            continue
        add(m ** 2 - n ** 2, 2 * m * n, m ** 2 + n ** 2)
    m += 1

kum = [0]
for i in cnt:
    kum.append(kum[-1] + len(i))
    if kum[-1] > 1000000:
        print(len(kum) - 2)
        break
