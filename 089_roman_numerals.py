utils = __import__('000_utils')

ans = 0
with open('089.in') as f:
    for line in f:
        cl = line.strip()
        ans += len(cl) - len(utils.itor(utils.rtoi(cl)))
print(ans)
