utils = __import__("000_utils")
N = 10000
p = utils.Prime(N).get_bool_arr()
for i in range(N - 1, 1, -1):
    if p[i]:
        j = 1
        while i + 2 * j ** 2 < N:
            p[i + 2 * j ** 2] = True
            j += 1
for i in range(3, N, 2):
    if not p[i]:
        print(i)
        break
