from math import factorial as f
utils = __import__('000_utils')
N = 1000000


def is_60(num):
    v = [num]
    for _ in range(59):
        n = utils.fact_digit_sum(v[-1])
        if n in v:
            return False
        v.append(n)
    return utils.fact_digit_sum(v[-1]) in v


ans = 0
for i in range(1, N + 1):
    ans += 1 if is_60(i) else 0
print(ans)
