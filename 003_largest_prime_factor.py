ans = 0
num = 600851475143
i=2

while i*i<=num:
    while num % i == 0:
        ans = max(ans, i)
        num //= i
    i += 1

ans = max(ans, num)
print(ans)
