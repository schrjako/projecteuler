Prime = __import__('000_primes').Prime


def s_number(num, target, s=0, split=False):
    if s + num < target or s > target:
        return False
    if target == s + num and split:
        return True
    c = 0
    f = 1
    while num > 0:
        c += num % 10 * f
        f *= 10
        num //= 10
        if s_number(num, target, s + c, split or num != 0):
            return True
    return False


def get_sqrt_approximations(num):
    from math import floor, gcd
    if is_square(num):
        return [[round(num ** (1 / 2))], [], []]
    b0 = floor(num ** (1 / 2))
    c0 = 1
    curr = '{}:{}'.format(b0, c0)
    d = []
    ret = [[b0], []]
    while curr not in d:
        d.append(curr)
        c1 = num - b0 ** 2
        g = gcd(c1, c0)
        c0 //= g
        c1 //= g
        if c0 != 1:
            print('c1 not 1, error')
            return -1
        a = (c0 * b0 + floor(num ** (1 / 2))) // c1
        ret[1].append(a)
        b1 = - (c0 * b0 - a * c1)
        b0 = b1
        c0 = c1
        curr = "{}:{}".format(b0, c0)
    ret.append(ret[1][d.index(curr):])
    ret[1] = ret[1][:d.index(curr)]
    return ret


def get_divisors(num):
    divisors = []
    i = 1
    while i ** 2 < num:
        if num % i == 0:
            divisors.append(i)
            divisors.append(num // i)
        i += 1
    if i ** 2 == num:
        divisors.append(i)
    return divisors


def array_sum(array):
    ret = 0
    for i in array:
        ret += i
    return ret


def digit_sum(num):
    ret = 0
    for i in str(num):
        ret += int(i)
    return ret


def fact_digit_sum(num):
    from math import factorial as f
    ret = 0
    for i in str(num):
        ret += f(int(i))
    return ret


def func_digit_sum(num, func):
    return sum(map(lambda n: func(int(n)), str(num)))


def sum_of_proper_divisors(num):
    return sum(get_divisors(num)) - num


def is_n_pandigital(num, n):
    snum = str(num)
    digits = "".join([str(i+1) for i in range(n)])
    for digit in digits:
        if snum.count(digit) != 1:
            return False
    return True


def is_pandigital(num):
    return is_n_pandigital(num, len(str(num)))


def is_palindrome(a):
    return str(a) == str(a)[::-1]


def word_value(word):
    ret = 0
    for char in word:
        ret += ord(char) - ord('A') + 1
    return ret


triangle = lambda n: n * (n + 1) // 2
r_triangle = lambda x: round((2 * x + 1 / 4) ** (1 / 2) - 1 / 2)
is_triangle = lambda x: triangle(r_triangle(x)) == x

square = lambda n: n ** 2
r_square = lambda x: round(x ** (1 / 2))
is_square = lambda x: square(r_square(x)) == x

pentagonal = lambda n: n * (3 * n - 1) // 2
r_pentagonal = lambda x: round(1 / 6 + (2 / 3 * x + 1 / 6) ** (1 / 2))
is_pentagonal = lambda x: pentagonal(r_pentagonal(x)) == x

hexagonal = lambda n: n * (2 * n - 1)
r_hexagonal = lambda x: round(1 / 4 + (x / 2 + 1 / 16) ** (1 / 2))
is_hexagonal = lambda x: hexagonal(r_hexagonal(x)) == x

heptagonal = lambda n: n * (5 * n - 3) // 2
r_heptagonal = lambda x: round(3 / 10 + (2 * x / 5 + 9 / 100) ** (1 / 2))
is_heptagonal = lambda x: heptagonal(r_heptagonal(x)) == x

octagonal = lambda n: n * (3 * n - 2)
r_octagonal = lambda x: round(1 / 3 + (x / 3 + 1 / 9) ** (1 / 2))
is_octagonal = lambda x: octagonal(r_octagonal(x)) == x

figurate = [0, 1, 2, triangle, square, pentagonal, hexagonal, heptagonal,
            octagonal]
r_figurate = [0, 1, 2, r_triangle, r_square, r_pentagonal, r_hexagonal,
              r_heptagonal, r_octagonal]
is_figurate = [0, 1, 2, is_triangle, is_square, is_pentagonal, is_hexagonal,
               is_heptagonal, is_octagonal]


def fast_mod_pow(base, power, mod):
    ret = 1
    while power > 0:
        if power % 2 == 1:
            ret *= base
            ret %= mod
        power //= 2
        base **= 2
        base %= mod
    return ret


def get_bin(n):
    b = [[1]]
    for i in range(n):
        b.append(
                [(0 if j == 0 else b[i][j - 1]) +
                 (0 if j == i + 1 else b[i][j]) for j in range(i + 2)])
    return b


def countchars(text, chars):
    ret = ''
    for char in chars:
        if str(text).count(char) != 0:
            ret += "{}:{}|".format(char, str(text).count(char))
    return ret


def rtoi(roman):
    chars = ['I', 'V', 'X', 'L', 'C', 'D', 'M']
    nums = [1, 5, 10, 50, 100, 500, 1000]
    ret = 0
    L = len(chars)
    for c in roman:
        if L < chars.index(c):
            ret -= 2 * nums[L]
        L = chars.index(c)
        ret += nums[L]
    return ret


def itor(i):
    chars = ['I', 'V', 'X', 'L', 'C', 'D', 'M']
    ret = ''
    j = 0
    while i > 0:
        c = i % 10
        if c in [1, 2, 3] or (c == 4 and j == len(chars) - 1):
            ret = chars[j] * c + ret
        elif c == 4:
            ret = chars[j] + chars[j + 1] + ret
        elif c in [5, 6, 7, 8]:
            ret = chars[j + 1] + chars[j] * (c - 5) + ret
        elif c == 9:
            ret = chars[j] + chars[j + 2] + ret
        i //= 10
        j += 2
    return ret
