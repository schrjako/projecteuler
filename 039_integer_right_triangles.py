num = [0]*1001

for a in range(1, 1000):
    for b in range(a, 1000):
        c = int((a ** 2 + b ** 2) ** (1/2))
        if c ** 2 == a ** 2 + b ** 2 and a + b + c <= 1000:
            num[a + b + c] += 1
ans = 0
for i in range(1001):
    if num[i] > num[ans]:
        ans = i
print(ans)
