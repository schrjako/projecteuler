utils = __import__("000_utils")
ans = 0
for i in range(1, 10000):
    num = i
    lycrel = True
    for j in range(55):
        num += int(str(num)[::-1])
        if utils.is_palindrome(num):
            lycrel = False
            break
    if lycrel:
        ans += 1
print(ans)
