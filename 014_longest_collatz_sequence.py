import sys
sys.setrecursionlimit(10000000)

memo = list(-1 for _ in range(100000000))
memo[1] = 1


def dfs(n):
    if n >= 100000000:
        if n % 2 == 0:
            return 1 + dfs(n//2)
        else:
            return 1 + dfs(3*n + 1)
    if memo[n] != -1:
        return memo[n]
    if n % 2 == 0:
        memo[n] = 1 + dfs(n//2)
    else:
        memo[n] = 1 + dfs(3*n + 1)
    return memo[n]


maxlen = 1

for i in range(1, 1000000):
    if dfs(i) > dfs(maxlen):
        maxlen = i

print(maxlen)
