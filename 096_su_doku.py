from copy import deepcopy as dcopy
from itertools import product


def rmfromset(it, val):
    if type(it) == set and val in it:
        it.remove(val)


def addnum(opts, num, x, y):
    for k in range(9):
        if opts[k][y] == num or opts[x][k] == num:
            return False
        rmfromset(opts[k][y], num)
        rmfromset(opts[x][k], num)
    bx = (x // 3) * 3
    by = (y // 3) * 3
    for dx, dy in product(range(3), repeat=2):
        if opts[bx + dx][by + dy] == num:
            return False
        rmfromset(opts[bx + dx][by + dy], num)
    opts[x][y] = num
    return True


def dfs(opts):
    change = True
    while change:
        change = False
        for x, y in product(range(9), repeat=2):
            if type(opts[x][y]) == set and len(opts[x][y]) == 1:
                change = True
                if not addnum(opts, opts[x][y].pop(), x, y):
                    return False, []
    M = 10
    for x, y in product(range(9), repeat=2):
        if type(opts[x][y]) == set and len(opts[x][y]) < M:
            M = len(opts[x][y])
            lx, ly = x, y
    if M == 10:
        return True, opts
    if M == 0:
        return False, []
    for num in opts[lx][ly]:
        tmpopts = dcopy(opts)
        if not addnum(tmpopts, num, lx, ly):
            continue
        ok, tmpopts = dfs(tmpopts)
        if ok:
            return True, tmpopts
    return False, []


def solve(sudoku):
    opts = [[set([i for i in range(1, 10)])
             for _ in range(9)] for _ in range(9)]
    for x, y in product(range(9), repeat=2):
        if sudoku[x][y] != '0':
            addnum(opts, int(sudoku[x][y]), x, y)
    ok, solved = dfs(opts)
    if not ok:
        print("Couldn't solve {}".format(sudoku))
    else:
        return int(''.join(map(lambda i: str(i), solved[0][:3])))
    return 0


ans = 0
with open('096.in') as f:
    sudokus = filter(lambda l: len(l) == 9,
                     map(lambda s: s.strip().split('\n')[1:],
                         f.read().split('Grid ')))
    for sudoku in sudokus:
        ans += solve(sudoku)
print(ans)
