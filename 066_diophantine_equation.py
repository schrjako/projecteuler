utils = __import__("000_utils")


def pell(num):
    a = utils.get_sqrt_approximations(num)
    h = [0, 1]
    k = [1, 0]
    for j in a:
        for i in j:
            h.append(i * h[-1] + h[-2])
            k.append(i * k[-1] + k[-2])
            if h[-1] ** 2 - num * k[-1] ** 2 == 1:
                return h[-1]
    while True:
        for i in a[2]:
            h.append(i * h[-1] + h[-2])
            k.append(i * k[-1] + k[-2])
            if h[-1] ** 2 - num * k[-1] ** 2 == 1:
                return h[-1]


N = 1001
ans = 2
M = 0
for d in range(2, N + 1):
    if utils.is_square(d):
        continue
    curr = pell(d)
    if curr > M:
        M = curr
        ans = d
print(ans)
