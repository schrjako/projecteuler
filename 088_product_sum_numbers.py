N = 12000
best = [0, 0] + [2 * k for k in range(2, N + 1)]


def dfs(num, prod, s, M):
    k = prod - s + num
    if k > N:
        return
    if k >= 2 and prod > 2:
        best[k] = min(best[k], prod)
    for i in range(2, M + 1):
        dfs(num + 1, prod * i, s + i, i)


dfs(0, 1, 0, N // 2)
print(sum(set(best)))
