import resource
from math import gcd
resource.setrlimit(resource.RLIMIT_STACK, (2**29, -1))
utils = __import__('000_utils')
N = 130000000
ans = 0
for p in range(3, N):
    for k in [-1, 1]:
        s = (3 * p + k)
        m = s * (s - 2 * p) ** 2 * (s - 2 * (p + k))
        if gcd(16, m) != 16:
            continue
        m //= 16
        if not utils.is_square(m):
            continue
        ans += s
print(ans)
