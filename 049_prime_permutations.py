utils = __import__("000_utils")
p = utils.Prime(10000).get_bool_arr()
for i in range(1000, 10000):
    if not p[i] or i == 1487:
        continue
    for j in range(i+2, 10000):
        if 2 * j - i >= 10000:
            break
        if not p[j] or not p[2 * j - i]:
            continue
        if set(str(i)) == set(str(j)) and set(str(i)) == set(str(2 * j - i)):
            print(str(i) + str(j) + str(2 * j - i))
            break
