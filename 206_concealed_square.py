def is_ok(num, L=0):
    s = str(num)
    for i in range(-3, L - 1, -2):
        if len(s)  < - L or int(s[i]) != 11 + i // 2:
            return False
    return True


def dfs(tail, i):
    if len(str(tail ** 2)) > 19:
        return -1
    if i == 10:
        if len(str(tail ** 2)) == 19 and is_ok(tail ** 2, -19):
            return tail
        return -1
    for j in range(9, -1, -1):
        nnum = tail + j * 10 ** i
        if is_ok(nnum ** 2, (i - 1) // 2 * (-2) - 3):
            ret = dfs(nnum, i + 1)
            if ret != -1:
                return ret
    return -1


print(dfs(0, 1))
