N = 56000
dp = [1] + [0] * N
for i in range(1, N + 1):
    for j in range(N + 1 - i):
        dp[j + i] += dp[j]
    if dp[i] % 1000000 == 0:
        print(i)
        break
