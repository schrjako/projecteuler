n = 100
squaresum = 0
sumsquare = ( (n * (n + 1)) / 2 ) ** 2

for i in range(n+1):
    squaresum += i**2

print(sumsquare - squaresum)
