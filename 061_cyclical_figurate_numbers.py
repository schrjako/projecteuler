utils = __import__("000_utils")
fig = []
N = 6
for i in range(N):
    j = 0
    fig.append({})
    while utils.figurate[i + 3](j) < 1000:
        j += 1
    while utils.figurate[i + 3](j) < 10000:
        f = utils.figurate[i + 3](j)
        if f // 100 not in fig[-1]:
            fig[-1][f // 100] = []
        fig[-1][f // 100].append(f % 100)
        j += 1
ans = -1


def dfs(nums, in_loop):
    # print(nums, in_loop)
    if len(nums) == N + 1:
        if nums[0] == nums[-1]:
            global ans
            ans = sum(nums[:-1]) * 101
        return
    for i in range(1, N):
        if in_loop[i] or nums[-1] not in fig[i]:
            continue
        in_loop[i] = True
        for num in fig[i][nums[-1]]:
            dfs(nums + [num], in_loop)
        in_loop[i] = False



in_loop = [True] + [False] * (N - 1)
for key in fig[0].keys():
    # print(key, fig[0][key])
    for num in fig[0][key]:
        dfs([key, num], in_loop)
print(ans)
