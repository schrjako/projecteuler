cnt = 0
n = 1
ans = 1
for i in range(1, 100000):
    cnt += len(str(i))
    if cnt >= n:
        ans *= int(str(i)[-(cnt-n+1)])
        n *= 10
    if n == 10000000:
        break
print(ans)
