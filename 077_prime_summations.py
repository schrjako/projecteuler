utils = __import__('000_utils')
N = 60000
P = utils.Prime(N)
dp = [1] + [0] * N
for p in P.get_primes_arr():
    for j in range(N + 1 - p):
        dp[j + p] += dp[j]
for j in range(N+1):
    if dp[j] > 5000:
        print(j)
        break
