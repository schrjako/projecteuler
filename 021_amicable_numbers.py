utils = __import__("000_utils")


def d(num):
    return utils.array_sum(utils.get_divisors(num)) - num


ans = 0
for i in range(1, 10000):
    if d(d(i)) == i and d(i) != i:
        ans += i

print(ans)
