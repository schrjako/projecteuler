def cycle_len(num):
    rests = {}
    rest = 1
    while rest % num != 0:
        if rest % num in rests:
            return len(rests) - rests[rest % num]
        rests[rest % num] = len(rests)
        rest %= num
        rest *= 10
    return 0


ans = 0
mlen = 0
for d in range(2, 1000):
    if cycle_len(d) > mlen:
        mlen = cycle_len(d)
        ans = d
print(ans)
