utils = __import__('000_utils')
N = 10 ** 6
v = [False] * N
v[0] = True
M = 0
ans = 0
for i in range(2, N):
    if v[i]:
        continue
    arr = []
    n = i
    while n not in arr and n < N and not v[n]:
        arr.append(n)
        n = utils.sum_of_proper_divisors(n)
    if n in arr and len(arr) - arr.index(n) > M:
        M = len(arr) - arr.index(n)
        ans = min(arr[arr.index(n):])
    for x in arr:
        v[x] = True
print(ans)
