
with open("022.in") as f:
    names = f.read().strip('\n').replace('"', '').split(',')

names.sort()
ans = 0
for i in range(len(names)):
    for char in names[i]:
        ans += (i+1) * (ord(char) - ord('A') + 1)
print(ans)
